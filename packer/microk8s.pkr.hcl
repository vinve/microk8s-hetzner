
variable "hcloud_token" {
  type    = string
  default = "${env("HCLOUD_TOKEN")}"
}

source "hcloud" "microk8s" {
  image       = "ubuntu-22.04"
  location    = "fsn1"
  server_type = "cax21"
  snapshot_labels = {
    base  = "ubuntu-22.04"
    build = "microk8s"
  }
  ssh_username = "root"
  token        = "${var.hcloud_token}"
}

build {
  sources = ["source.hcloud.microk8s"]

  provisioner "shell" {
    inline = [
      "while fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; do sleep 1; done",
      "echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections",
      "apt-get update",
      "apt-get -qy dist-upgrade",
      "apt-get install -qy apt-transport-https curl unattended-upgrades snapd",
      "echo \"alias kubectl='microk8s kubectl'\" >> /root/.bash_aliases",
      "ufw allow in on eth0 to any port 22 proto tcp",
      "ufw --force enable",
      "ufw delete allow in on eth0 to any port 22 proto tcp"
    ]
  }

  provisioner "shell" {
    inline = [
      "sed -i 's/^#Port 22/Port 4442/' /etc/ssh/sshd_config",
      "ufw allow in on eth0 to any port 4442 proto tcp",
      "apt-get autoremove",
      "apt-get clean",
      "cat /dev/null > /etc/machine-id",
      "rm -f /etc/ssh/ssh_host_*"
    ]
  }
}

packer {
  required_plugins {
    hcloud = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/hcloud"
    }
  }
}
