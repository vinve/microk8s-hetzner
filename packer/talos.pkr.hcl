
variable "talos_version" {
  type    = string
  default = "v1.4.1"
}

variable "hcloud_token" {
  type    = string
  default = "${env("HCLOUD_TOKEN")}"
}

locals {
  image = "https://github.com/siderolabs/talos/releases/download/${var.talos_version}/hcloud-arm64.raw.xz"
}

source "hcloud" "talos" {
  rescue       = "linux64"
  image        = "debian-11"
  location     = "fsn1"
  server_type  = "cax11"
  snapshot_labels = {
    build = "talos"
    version = "${var.talos_version}",
  }
  ssh_username = "root"
  token        = "${var.hcloud_token}"
}

build {
  sources = ["source.hcloud.talos"]

  provisioner "shell" {
    inline = [
      "apt-get install -y wget",
      "wget -O /tmp/talos.raw.xz ${local.image}",
      "xz -d -c /tmp/talos.raw.xz | dd of=/dev/sda && sync",
    ]
  }
}

packer {
  required_plugins {
    hcloud = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/hcloud"
    }
  }
}
