provider "hcloud" {
  token = var.api_token
}

resource "hcloud_ssh_key" "default" {
  name       = "default"
  public_key = file("~/.ssh/hcloud.pub")
}

resource "hcloud_placement_group" "microk8s" {
  name = "microk8s"
  type = "spread"
  labels = {
    "cluster" = "microk8s"
  }
}

resource "hcloud_server" "node" {
  count = var.number_of_nodes

  name               = "nod${format("%02d", count.index + 1)}"
  image              = var.node_image
  server_type        = var.node_flavor
  location           = "fsn1"
  ssh_keys           = [hcloud_ssh_key.default.id]
  placement_group_id = hcloud_placement_group.microk8s.id
  backups            = false

  labels = {
    "cluster"  = "microk8s",
    "location" = "fsn1"
  }

  provisioner "local-exec" {
    command = "sleep 20"
  }
}
