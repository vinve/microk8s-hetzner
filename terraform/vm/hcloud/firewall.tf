resource "hcloud_firewall" "microk8s" {
  name = "microk8s-fw"
  labels = {
    "cluster" = "microk8s"
  }

  rule {
    direction = "in"
    protocol  = "icmp"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "4442"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  apply_to {
    label_selector = "cluster=microk8s"
  }
}
