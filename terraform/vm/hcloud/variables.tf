variable "api_token" {
  type      = string
  sensitive = true
}

variable "number_of_nodes" {
  type = number
}

variable "node_image" {
  type = string
}

variable "node_flavor" {
  type    = string
  default = "cax21"
}

variable "private_nic" {
  type    = string
  default = "enp7s0"
}

variable "custom_ssh_port" {
  type    = number
  default = 4442
}
