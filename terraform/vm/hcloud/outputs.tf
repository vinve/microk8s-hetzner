output "node_ips" {
  value = hcloud_server.node.*.ipv6_address
}

output "node_names" {
  value = hcloud_server.node.*.name
}

output "node_locations" {
  value = hcloud_server.node.*.location
}

output "node_datacenters" {
  value = hcloud_server.node.*.datacenter
}

output "private_nic" {
  value = var.private_nic
}

output "node_private_ips" {
  value = hcloud_server_network.node_network.*.ip
}

output "custom_ssh_port" {
  value = var.custom_ssh_port
}
