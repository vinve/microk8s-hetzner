resource "hcloud_network" "internal" {
  name     = "internal"
  ip_range = "10.0.1.0/24"
  labels = {
    "cluster" = "microk8s"
  }
}

resource "hcloud_network_subnet" "k8s_subnet" {
  network_id   = hcloud_network.internal.id
  type         = "server"
  network_zone = "eu-central"
  ip_range     = "10.0.1.0/27"
}

resource "hcloud_server_network" "node_network" {
  count = var.number_of_nodes

  server_id  = element(hcloud_server.node.*.id, count.index)
  network_id = hcloud_network.internal.id
  ip         = "10.0.1.${count.index + 11}"

  provisioner "remote-exec" {
    inline = [
      "ufw allow in on ${var.private_nic}"
    ]

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = element(hcloud_server.node.*.ipv6_address, count.index)
    }
  }
}
