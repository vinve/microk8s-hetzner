module "vm" {
  source = "./vm/hcloud"

  api_token       = var.api_token
  node_image      = var.node_image
  node_flavor     = var.node_flavor
  number_of_nodes = var.number_of_nodes
}

module "k8s" {
  source = "./k8s/microk8s"

  api_token        = var.api_token
  node_ips         = module.vm.node_ips
  node_private_ips = module.vm.node_private_ips
  node_names       = module.vm.node_names
  node_locations   = module.vm.node_locations
  node_datacenters = module.vm.node_datacenters
  private_nic      = module.vm.private_nic
  custom_ssh_port  = module.vm.custom_ssh_port
}
