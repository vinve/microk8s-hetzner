#!/bin/bash
KUBECTL="microk8s kubectl"
GITOPS_GIT_REPO="https://gitlab.com/vinve/microk8s-gitops-config.git"

# Create temporary directory
TEMP_DIR=$(mktemp -d)
cd ${TEMP_DIR} || exit 1

${KUBECTL} create namespace argocd
${KUBECTL} apply -n argocd -f \
  https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
sleep 10
${KUBECTL} wait --for=condition=ready po -l app.kubernetes.io/name=argocd-server \
  -n argocd --timeout=500s

# Add ArgoCD ApplicationSet
cat > application_set <<EOF
apiVersion: argoproj.io/v1alpha1
kind: ApplicationSet
metadata:
  name: gitops-config
  namespace: argocd
spec:
  generators:
  - git:
      repoURL: "${GITOPS_GIT_REPO}"
      revision: main
      directories:
      - path: clusters/hetzner.production/*
  template:
    metadata:
      name: '{{path.basename}}'
      namespace: argocd
    spec:
      project: default
      source:
        repoURL: "${GITOPS_GIT_REPO}"
        targetRevision: main
        path: '{{path}}'
      destination:
        server: https://kubernetes.default.svc
      syncPolicy:
        automated:
          selfHeal: true
          prune: true
EOF

${KUBECTL} apply -f application_set

cd /tmp
rm -fr ${TEMP_DIR}
