variable "api_token" {
  type      = string
  sensitive = true
}

variable "custom_ssh_port" {
  type = string
}

variable "node_ips" {
  type = list(string)
}

variable "node_private_ips" {
  type = list(string)
}

variable "private_nic" {
  type = string
}

variable "node_names" {
  type = list(string)
}

variable "node_locations" {
  type = list(string)
}

variable "node_datacenters" {
  type = list(string)
}
