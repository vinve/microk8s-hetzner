resource "null_resource" "create_csi_secret" {
  triggers = {
    node_ip = "${local.first_node_ip}"
  }

  provisioner "remote-exec" {
    inline = [
      "microk8s kubectl create secret generic hcloud -n kube-system --from-literal=token=${var.api_token}"
    ]

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = local.first_node_ip
    }
  }

  depends_on = [null_resource.label_nodes_topology]
}
