resource "random_string" "join_token" {
  length  = 32
  upper   = false
  special = false
}

locals {
  first_node_ip         = element(var.node_ips, 0)
  first_node_private_ip = element(var.node_private_ips, 0)
  join_token            = random_string.join_token.result
}

resource "null_resource" "install_microk8s" {
  count = length(var.node_ips)

  triggers = {
    node_ip = element(var.node_ips, count.index)
  }

  provisioner "remote-exec" {
    inline = [
      "ufw default allow routed",
      "ufw allow in on vxlan.calico",
      "ufw allow out on vxlan.calico",
      "ufw allow in on cali+",
      "ufw allow out on cali+",
      "snap install microk8s --classic --channel=latest/stable",
      "microk8s status --wait-ready"
    ]

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = element(var.node_ips, count.index)
    }
  }
}

resource "null_resource" "init_first_node" {
  triggers = {
    node_ips = join(",", var.node_ips)
  }

  provisioner "remote-exec" {
    inline = [
      "microk8s enable dns ingress",
      "microk8s add-node -t ${local.join_token} --token-ttl 3600"
    ]

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = local.first_node_ip
    }
  }

  depends_on = [null_resource.install_microk8s]
}

resource "null_resource" "join_nodes" {
  count = length(var.node_ips) - 1

  triggers = {
    first_node_private_ip = local.first_node_private_ip
    node_ip               = element(var.node_ips, count.index)
  }

  provisioner "remote-exec" {
    inline = [
      "microk8s join ${local.first_node_private_ip}:25000/${local.join_token}",
      "microk8s status --wait-ready",
      "sleep 30",
    ]

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = element(var.node_ips, count.index + 1)
    }
  }

  depends_on = [null_resource.update_etc_hosts]
}

resource "null_resource" "set_node_ip_private_network" {
  count = length(var.node_ips)

  triggers = {
    node_private_ip = element(var.node_private_ips, count.index)
  }

  provisioner "remote-exec" {
    inline = [
      "microk8s.stop",
      "echo --node-ip=${element(var.node_private_ips, count.index)} >> /var/snap/microk8s/current/args/kubelet",
      "echo --advertise-address=${element(var.node_private_ips, count.index)} >> /var/snap/microk8s/current/args/kube-apiserver",
      "microk8s.start",
      "microk8s status --wait-ready",
      "sleep 60"
    ]

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = element(var.node_ips, count.index)
    }
  }

  depends_on = [null_resource.join_nodes]
}

resource "null_resource" "label_nodes_topology" {
  count = length(var.node_ips)

  triggers = {
    node_name       = element(var.node_names, count.index)
    node_location   = element(var.node_locations, count.index)
    node_datacenter = element(var.node_datacenters, count.index)
    node_ips        = join(", ", var.node_ips)
  }

  provisioner "remote-exec" {
    inline = [
      "microk8s kubectl label --overwrite node ${element(var.node_names, count.index)} topology.kubernetes.io/zone=${element(var.node_datacenters, count.index)}",
      "microk8s kubectl label --overwrite node ${element(var.node_names, count.index)} topology.kubernetes.io/region=${element(var.node_locations, count.index)}"
    ]

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = local.first_node_ip
    }
  }

  depends_on = [null_resource.set_node_ip_private_network]
}

resource "null_resource" "update_etc_hosts" {
  count = length(var.node_ips)

  triggers = {
    node_names = join(",", var.node_names)
  }

  provisioner "remote-exec" {
    script = "k8s/microk8s/scripts/update_etc_hosts.sh"

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = element(var.node_ips, count.index)
    }
  }

  depends_on = [null_resource.init_first_node]
}
