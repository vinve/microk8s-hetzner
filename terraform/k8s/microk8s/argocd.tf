resource "null_resource" "deploy_argocd" {
  triggers = {
    first_node_ip = local.first_node_ip
  }

  provisioner "remote-exec" {
    script = "k8s/microk8s/scripts/argocd.sh"

    connection {
      port        = var.custom_ssh_port
      private_key = file("~/.ssh/hcloud")
      host        = local.first_node_ip
    }
  }

  depends_on = [null_resource.create_csi_secret]
}
