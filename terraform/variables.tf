variable "api_token" {
  type      = string
  sensitive = true
}

variable "node_image" {
  type = string
}

variable "number_of_nodes" {
  type = number
}

variable "node_flavor" {
  type = string
}
