# microk8s-hetzner

Code to set up a microk8s cluster on Hetzner Cloud.

* https://microk8s.io/docs/
* https://www.hetzner.com/cloud
